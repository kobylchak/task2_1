package ua.kob.vas;

import lombok.*;

import javax.xml.bind.annotation.*;
import java.util.*;
import java.util.stream.Collectors;

@Setter
@ToString
@EqualsAndHashCode
@XmlRootElement(name = "trains")
public class Trains {
    @XmlElement(name = "train")
    private List<Train> trainList = new ArrayList<>();

    public void add(Train train) {
        trainList.add(train);
    }

    public List<Train> filterTrainListByTime() {
        return trainList.stream()
                .filter(train -> Integer.parseInt(train.getDeparture().substring(0, 2)) >= 15 &&
                        Integer.parseInt(train.getDeparture().substring(0, 2)) <= 16)
                .collect(Collectors.toCollection(ArrayList::new));
    }
}
