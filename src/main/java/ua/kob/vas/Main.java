package ua.kob.vas;

import java.io.File;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Trains trains = JAXB.unmarsh(new File("trains.xml"));
        trains.add(new Train("Kiev", "Odessa", "02-09-2018", "15:20", 47));
        List<Train> trainList = trains.filterTrainListByTime();
        for (Train train : trainList) {
            System.out.println(train);
        }
    }
}
