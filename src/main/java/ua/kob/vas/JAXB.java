package ua.kob.vas;

import java.io.File;
import javax.xml.bind.*;

public class JAXB {
    public static Trains unmarsh(File file) {
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(Trains.class);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            Trains trains = (Trains) unmarshaller.unmarshal(file);
            return trains;
        } catch (JAXBException e) {
            System.out.println(e.getMessage());
        }
        return null;
    }
}
