package ua.kob.vas;

import lombok.*;

import javax.xml.bind.annotation.*;

@Getter
@ToString
@XmlRootElement(name = "train")
public class Train {
    private String from;
    private String to;
    private String date;
    private String departure;
    @XmlAttribute
    private int id;

    public Train() {
    }

    public Train(String from, String to, String date, String departure, int id) {
        this.from = from;
        this.to = to;
        this.date = date;
        this.departure = departure;
        this.id = id;
    }

    @XmlElement
    public void setFrom(String from) {
        this.from = from;
    }

    @XmlElement
    public void setTo(String to) {
        this.to = to;
    }

    @XmlElement
    public void setDate(String date) {
        this.date = date;
    }

    @XmlElement
    public void setDeparture(String departure) {
        this.departure = departure;
    }
}
